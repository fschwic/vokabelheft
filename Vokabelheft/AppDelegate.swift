//
//  AppDelegate.swift
//  Vokabelheft
//
//  Created by Frank Schwichtenberg on 26.11.20.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    static public var currentSection:Int = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Domain Data Handling
    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "de_DE")
        formatter.dateStyle = DateFormatter.Style.short
        return formatter
    }
    
    struct DaySection {
        var day:Date
        var terms:[Term]
    }

    static func fetchSections() -> [DaySection] {
        var sections:[DaySection] = []
        let vocabulary:[Term] = AppDelegate.fetchAll()

        func startOfDay(date: Date) -> Date {
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day], from: date)
            return calendar.date(from: components)!
        }
        
        let groups = Dictionary(grouping: vocabulary) { (term) in
            return startOfDay(date: term.timestamp ?? Date())
        }
        sections = groups.map(DaySection.init(day:terms:))
        sections.sort(by: {(lowerDaySection, higherDaySection) in lowerDaySection.day > higherDaySection.day})
        
        return sections
    }

    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentCloudKitContainer {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    }
    
    static func saveContext() {
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    static func fetchAll() -> [Term] {
        
        let request : NSFetchRequest<Term> = Term.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
     
        guard let terms = try? persistentContainer.viewContext.fetch(request)
            else {
                return []
            }
        
        return terms
    }
    
    static func deleteAll() {
        fetchAll().forEach({ persistentContainer.viewContext.delete($0) })
        try? persistentContainer.viewContext.save()
    }
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        export()
        importReplace()
        return true
    }
    
    func export(){
        do {
            NSLog("Exporting terms ...")
            let terms = AppDelegate.fetchAll()
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try encoder.encode(terms)
            //NSLog(String(data: data, encoding: .utf8) ?? "no encoding")
            
            let fileName = "Vokabeln"
            let documentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                    
            let fileURL = documentDirURL.appendingPathComponent(fileName).appendingPathExtension("json")
            //print("FilePath: \(fileURL.path)")
                    
            let writeString: String = String(data: data, encoding: .utf8) ?? "empty"
            do {
                // Write to the file
                try writeString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            } catch let error as NSError {
                NSLog("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
            }
            
            NSLog("... done.")
        } catch {
            NSLog("encoding failed")
            let nserror = error as NSError
            NSLog(nserror.description)
        }
    }
    
    func importReplace(){
        
        // Import from file if exists
        do {
            NSLog("Trying to import terms ...")
            let fileName = "VokabelnImport"
            let fileDoneName = "VokabelnImportDone"
            let fileManager = FileManager.default
            let DocumentDirURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    
            let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("json")
            let fileDoneURL = DocumentDirURL.appendingPathComponent(fileDoneName).appendingPathExtension("json")
            print("FilePath: \(fileURL.path)")

            if fileManager.fileExists(atPath: fileURL.path) {
                let data = try Data(contentsOf: fileURL)
                let decoder = JSONDecoder()
                
                // Delete everything without saving context
                // deleteAll()
                AppDelegate.fetchAll().forEach({ persistentContainer.viewContext.delete($0) })

                // Put terms from import file into context
                do {
                    let terms: [Term] = try decoder.decode([Term].self, from: data)
                    NSLog(terms.description)
                    NSLog("... done.")
                } catch {
                    NSLog("Error decoding import.")
                }

                // If loading and decoding successful, save context.
                try? persistentContainer.viewContext.save()
                
                // remove import file in order not to import next time again
                do {
                    try fileManager.moveItem(atPath: fileURL.path, toPath: fileDoneURL.path)
                } catch {
                    NSLog("Moving import file away failed.")
                }

            }
            else {
                NSLog("No import file found.")
            }

        } catch {
            NSLog("Error in Import")
            let nserror = error as NSError
            NSLog(nserror.description)
        }

    }
    
    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "Vokabelheft")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        debugPrint("didReceiveRemoteNotification")
        debugPrint(userInfo.description)
    }
    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


}

