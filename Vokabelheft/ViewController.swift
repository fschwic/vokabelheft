//
//  ViewController.swift
//  Vokabelheft
//
//  Created by Frank Schwichtenberg on 26.11.20.
//

import UIKit
import CoreData

class ViewController: UITableViewController {
    
    @IBOutlet weak var enTermTextField: UITextField!
    @IBOutlet weak var deTermTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    let regexMultiWhitespace = try! NSRegularExpression(pattern: "\\w\\w+")
    
    //var vocabulary:[Term] = AppDelegate.fetchAll()
    var sections:[AppDelegate.DaySection] = AppDelegate.fetchSections()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func deTermTextFieldEditingEnd(_ sender: UITextField) {
        addNewTerm()
    }
    
    @IBAction func addButtonTouchUpI(_ sender: UIButton) {
        addNewTerm()
    }

    func addNewTerm(){
        let enTermText = enTermTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        let deTermText = deTermTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        if !(enTermText?.isEmpty ?? true) && !(deTermText?.isEmpty ?? true) {
            // create new term in context
            let term = Term(context: AppDelegate.persistentContainer.viewContext)
            term.en = enTermText
            term.de = deTermText
            term.timestamp = Date()
            enTermTextField.text = ""
            deTermTextField.text = ""
            syncContext()
        }
    }
    
    func syncContext(){
        AppDelegate.saveContext()
        //vocabulary = AppDelegate.fetchAll()
        sections = AppDelegate.fetchSections()
        tableView.reloadData()
    }
    
    // TODO duplicated in UebenView, SchreibenView
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "de_DE")
        formatter.dateStyle = DateFormatter.Style.short
        return formatter
    }
    
    private func startOfDay(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        return calendar.date(from: components)!
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let day = sections[section].day
        return dateFormatter.string(from: day)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].terms.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VocCell", for: indexPath)
        
        //let term = vocabulary[indexPath.row]
        let term = sections[indexPath.section].terms[indexPath.row]
        cell.textLabel?.text = term.en//! + term.timestamp!.description
        cell.detailTextLabel?.text = "\(term.de!)"//", \(term.timestamp ?? Date()), \(term.right), \(term.wrong)"

        /*
        let calendar = Calendar.current
        let year = calendar.component(Calendar.Component.year, from: term.timestamp!)
        let month = calendar.component(Calendar.Component.month, from: term.timestamp!)
        let day = calendar.component(Calendar.Component.day, from: term.timestamp!)

        let session =
        (context: AppDelegate.persistentContainer.viewContext)
        session.day = Date.init()
        */
        
        return cell
    }

    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            //tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            // if no sections: AppDelegate.persistentContainer.viewContext.delete(vocabulary.remove(at: indexPath.row))
            AppDelegate.persistentContainer.viewContext.delete(sections[indexPath.section].terms.remove(at: indexPath.row))
            syncContext()
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let editView:EditTermViewController = segue.destination as? EditTermViewController else {
            return
        }
        guard let selectedCell = sender as? UITableViewCell else {
            fatalError("Is not UITableViewCell")
        }
        guard let selectedCellIndexPath = tableView.indexPath(for: selectedCell) else {return}
        let term = sections[selectedCellIndexPath.section].terms[selectedCellIndexPath.row]
        
        editView.term = term
    }

    @IBAction func unwindToTermList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? EditTermViewController {
            syncContext()
        }
    }
}

