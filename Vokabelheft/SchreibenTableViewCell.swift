//
//  SchreibenTableViewCell.swift
//  Vokabelheft
//
//  Created by Frank Schwichtenberg on 13.12.20.
//

import UIKit
import CoreData

class SchreibenTableViewCell: UITableViewCell {
    struct State {
        var written: String
        var solved: Solved 
    }
    enum Solved {
        case notyet
        case right
        case wrong
    }

    var term: Term?
    var written: String = ""
    var solved: Solved = Solved.notyet {
        didSet {
            if oldValue != solved {
                setNeedsUpdateConfiguration()
            }
        }
    }
    @IBOutlet weak var schreibenLabel: UILabel!
    @IBOutlet weak var schreibenTextField: UITextField!
    @IBOutlet weak var schreibenButton: UIButton!
    
    // MARK: - Custom State Extension
    
    override var configurationState: UICellConfigurationState {
        var state = super.configurationState
        state.solved = self.solved
        
        return state
    }
    
    override func prepareForReuse() {
        solved = Solved.notyet
        schreibenTextField.text = ""
        //schreibenTextField.backgroundColor = UIColor.systemGray3
        super.prepareForReuse()
    }
    
    override func updateConfiguration(using state: UICellConfigurationState) {
        written = term?.state?.written ?? ""
        solved = term?.state?.solved ?? Solved.notyet
        
        var backgroundConfig = UIBackgroundConfiguration.listPlainCell().updated(for: state)
        //var contentConfig = self.defaultContentConfiguration().updated(for: state)
        
        if solved == Solved.right {
            backgroundConfig.backgroundColor = UIColor.systemGreen.withAlphaComponent(0.5)
            //self.schreibenTextField.backgroundColor = UIColor.systemGreen.withAlphaComponent(0.5)
            self.schreibenTextField.text = written
        }
        else if solved == Solved.wrong {
            backgroundConfig.backgroundColor = UIColor.systemRed.withAlphaComponent(0.5)
            //self.schreibenTextField.backgroundColor = UIColor.systemRed.withAlphaComponent(0.5)
            self.schreibenTextField.text = written
        }

        //self.backgroundConfiguration = contentConfig
        self.backgroundConfiguration = backgroundConfig
    }

    // Mark: - Actions from Cell
    
    @IBAction func schreibenButtonTouch(_ sender: UIButton) {
        let written = self.schreibenTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        //let indexPath: IndexPath = [(UITableView)self.superview indexPathForCell: self]
        if !(written?.isEmpty ?? false) {
            self.written = written!
            
            let letters: CharacterSet = CharacterSet.letters.union(CharacterSet.whitespaces).union(CharacterSet.init(charactersIn: ",.!?"))
            let writtenChopped = written!.replaceCharactersFromSet(characterSet: letters.inverted, replacementString: "_")
            let selfTermEnChopped = self.term?.en?.replaceCharactersFromSet(characterSet: letters.inverted, replacementString: "_")
            
            if writtenChopped == selfTermEnChopped {
                self.solved = Solved.right
                term?.state = State(written: written!, solved: solved)
                //self.schreibenTextField.backgroundColor = UIColor.systemGreen.withAlphaComponent(0.5)
                self.term?.right += 1
            }
            else {
                self.solved = Solved.wrong
                term?.state = State(written: written!, solved: solved)
                //self.schreibenTextField.backgroundColor = UIColor.systemRed.withAlphaComponent(0.5)
                self.term?.wrong += 1
            }

            AppDelegate.saveContext()
        }
        
    }
}

// Mark: - Extensions
extension String {
    func replaceCharactersFromSet(characterSet: CharacterSet, replacementString: String = "") -> String {
        return components(separatedBy: characterSet).joined(separator: replacementString)
    }
}

extension UIConfigurationStateCustomKey {
    static let solved = UIConfigurationStateCustomKey("net.schwichtenberg.apps.Vokabelheft.SchreibenCell.soved")
}

extension UICellConfigurationState {
    
    var solved: SchreibenTableViewCell.Solved {
        get { return self[.solved] as? SchreibenTableViewCell.Solved ?? SchreibenTableViewCell.Solved.notyet }
        set { self[.solved] = newValue }
    }

}
