//
//  EditTermViewController.swift
//  Vokabelheft
//
//  Created by Frank Schwichtenberg on 14.04.21.
//

import UIKit
import CoreData

class EditTermViewController: UIViewController {
    
    var term: Term?
    var dataChanged = false
    
    @IBOutlet weak var EnTermTextField: UITextField!
    @IBOutlet weak var DeTermTextField: UITextField!
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var OkButton: UIButton!
    
    @IBAction func saveTerm(_ sender: Any) {
        /*
        term?.en = EnTermTextField.text
        term?.de = DeTermTextField.text
        AppDelegate.saveContext()
        dataChanged = true
        self.dismiss(animated: true, completion: nil)
        */
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        EnTermTextField.text = term?.en
        DeTermTextField.text = term?.de
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIButton, button === OkButton else {
            NSLog("The save button was not pressed, cancelling")
            return
        }
        term?.en = EnTermTextField.text
        term?.de = DeTermTextField.text
    }
}
