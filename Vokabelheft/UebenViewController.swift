//
//  UebenViewController.swift
//  Vokabelheft
//
//  Created by Frank Schwichtenberg on 02.12.20.
//

import UIKit
import CoreData

class UebenViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var vocabulary:[Term] = [] {
        didSet {
            self.vocabulary.sort(by: {(prev,next) in prev.tried < next.tried})
        }
    }
    var sections:[AppDelegate.DaySection] = AppDelegate.fetchSections()
    var toEn = true
    
    @IBOutlet weak var datePicker: UIPickerView!
    
    @IBAction func turnButton(_ sender: UIBarButtonItem) {
        if toEn {
            toEn = false
        }
        else{
            toEn = true
        }
        tableView.reloadData()
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.delegate = self
        datePicker.dataSource = self
        datePicker.selectRow(AppDelegate.currentSection, inComponent: 0, animated: true)
        if !sections.isEmpty {
            vocabulary = sections[AppDelegate.currentSection].terms
        }
    }
}

extension UebenViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sections.count
    }
    
    // TODO duplicated in View
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "de_DE")
        formatter.dateStyle = DateFormatter.Style.short
        return formatter
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let day = sections[row].day
        return dateFormatter.string(from: day)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        AppDelegate.currentSection = row
        vocabulary = sections[row].terms
        tableView.reloadData()
    }
    

    
}

extension UebenViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vocabulary.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: .default, reuseIdentifier: "UebenVocCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "UebenVocCell", for: indexPath)
        let term = vocabulary[indexPath.row]
        //let timestampString: String = term.timestamp?.description ?? ""
        let shownterm: String
        if toEn {
            shownterm = term.de ?? "unbestimmt"
        }
        else{
            shownterm = term.en ?? "unknown"
        }
        cell.textLabel?.text = "\(shownterm)"//" \(timestampString)"
        cell.detailTextLabel?.isEnabled = false
        cell.detailTextLabel?.text = "\(term.tried) Mal geübt"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "UebenVocCell", for: indexPath)
        let cell = tableView.cellForRow(at: indexPath)
        let term = vocabulary[indexPath.row]

        let hiddenterm: String
        if toEn {
            hiddenterm = term.en ?? "unknown"
        }
        else{
            hiddenterm = term.de ?? "unbestimmt"
        }

        if cell?.detailTextLabel?.text != hiddenterm {
            cell?.detailTextLabel?.isEnabled = true
            cell?.detailTextLabel?.text = "\(hiddenterm)"
        
            term.tried += 1
            AppDelegate.saveContext()
        }
    }
}
