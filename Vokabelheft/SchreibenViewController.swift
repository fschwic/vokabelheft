//
//  SchreibenViewController.swift
//  Vokabelheft
//
//  Created by Frank Schwichtenberg on 07.12.20.
//

import UIKit
import CoreData

class SchreibenViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var vocabulary:[Term] = [] {
        didSet {
            self.vocabulary.sort(by: {(prev,next) in prev.wrong > next.wrong})
        }
    }
    var sections:[AppDelegate.DaySection] = AppDelegate.fetchSections()
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        vocabulary.forEach { term in
            term.state = nil
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dayPicker.delegate = self
        dayPicker.dataSource = self
        dayPicker.selectRow(AppDelegate.currentSection, inComponent: 0, animated: true)
        if !sections.isEmpty {
            vocabulary = sections[AppDelegate.currentSection].terms
        }
    }
    @IBOutlet weak var dayPicker: UIPickerView!
}

extension SchreibenViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sections.count
    }
    
    // TODO duplicated in View
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "de_DE")
        formatter.dateStyle = DateFormatter.Style.short
        return formatter
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let day = sections[row].day
        return dateFormatter.string(from: day)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        AppDelegate.currentSection = row
        vocabulary = sections[row].terms
        tableView.reloadData()
    }
}

extension SchreibenViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vocabulary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchreibenVocCell", for: indexPath) as! SchreibenTableViewCell
        
        cell.term = vocabulary[indexPath.row]
        cell.schreibenLabel.text = "\(cell.term?.de ?? "Term")"//" Row \(indexPath.row)"

        return cell
    }
    
    
}
