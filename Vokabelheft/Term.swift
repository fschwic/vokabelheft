//
//  Term+CoreDataClass.swift
//
//
//  Created by Frank Schwichtenberg on 08.12.20.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

public class Term: NSManagedObject, Encodable, Decodable {
    
    enum CodingKeys: CodingKey {
        case de, en, timestamp, tried, right, wrong
      }
    
    var state: SchreibenTableViewCell.State?
    
    required convenience public init(from decoder: Decoder) throws {
        let context = AppDelegate.persistentContainer.viewContext
        
        self.init(context: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.de = try container.decode(String.self, forKey: .de)
        self.en = try container.decode(String.self, forKey: .en)
        //self.timestamp = try Date(from: decoder)
        self.timestamp = try container.decode(Date.self, forKey: .timestamp)
        //self.timestamp = try container.decode(JSONDecoder.DateDecodingStrategy, forKey: .timestamp)
        self.tried = try container.decode(Int16.self, forKey: .tried)
        self.right = try container.decode(Int16.self, forKey: .right)
        self.wrong = try container.decode(Int16.self, forKey: .wrong)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(de, forKey: .de)
        try container.encode(en, forKey: .en)
        try container.encode(timestamp, forKey: .timestamp)
        try container.encode(tried, forKey: .tried)
        try container.encode(right, forKey: .right)
        try container.encode(wrong, forKey: .wrong)
    }
}
